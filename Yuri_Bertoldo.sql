-- CRIANDO O DATABASE

CREATE DATABASE COVID
ON
( NAME = COVID,
    FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\saledat.mdf',
    SIZE = 1000,
    MAXSIZE = 1000,
    FILEGROWTH = 5 )
LOG ON
( NAME = COVID,
    FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\salelog.ldf',
    SIZE = 500MB,
    MAXSIZE = 500MB,
    FILEGROWTH = 5MB );
GO

-- CRIANDO AS TABELAS

USE COVID
GO

CREATE TABLE CONFIRMED
(
'Province/State'		VARCHAR (50),
'Country/Region'		VARCHAR (100)					NOT NULL,
'Lat'			        VARCHAR (10),
'Long'			        VARCHAR (10),
'Date'	                DATETIME                        NOT NULL,
'Valor'		            INT
)

CREATE TABLE Deaths Recovered
(
'Province/State'		VARCHAR (50),
'Country/Region'		VARCHAR (100)					NOT NULL,
'Lat'			        VARCHAR (10),
'Long'			        VARCHAR (10),
'Date'	                DATETIME                        NOT NULL,
'Valor'		            INT
)

CREATE TABLE Recovered
(
'Province/State'		VARCHAR (50),
'Country/Region'		VARCHAR (100)					NOT NULL,
'Lat'			        VARCHAR (10),
'Long'			        VARCHAR (10),
'Date'	                DATETIME                        NOT NULL,
'Valor'		            INT
)

-- SUBINDO DOS DADOS DO CSV

USE COVID;
GO

BULK INSERT CONFIRMED FROM 'D:\Downloads\...'
   WITH (
      FIELDTERMINATOR = ';',
      ROWTERMINATOR = '\n',
	  FIRSTROW = 2,
	  KEEPIDENTITY